<html>

<head>
  <meta charset="UTF-8" />
  <title>Document</title>
</head>

<body>
  <h3>Matkalaskuri</h3>
  <?php
  $mailit = filter_input(INPUT_POST, "mailit", FILTER_SANITIZE_NUMBER_INT);
  $kilometrit = $mailit * 1.609;
  printf("%d mailia on %.2f kilometriä", $mailit, $kilometrit);
  ?>
  <a href="index.html">Laske uudestaan</a>
</body>

</html>