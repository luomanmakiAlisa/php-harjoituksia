<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h3>Laina</h3>
    <?php
    $summa = filter_input(INPUT_POST, "paaoma", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $korko = filter_input(INPUT_POST, "korko", FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $aika = filter_input(INPUT_POST, "aika", FILTER_SANITIZE_NUMBER_INT);
    $kkkorko = $korko / 100;
    $kuukaudet = $aika * 12;
    $jaettava = $kkkorko / 12 * pow(1 + $kkkorko / 12, $kuukaudet);
    $jakaja = pow(1 + $kkkorko / 12, $kuukaudet) - 1;
    $lyhennys = $jaettava / $jakaja * $summa;
    printf("<p>Lainan maksu kuukaudessa %.2f</p>", $lyhennys);
    ?>
    <a href="index.php">Laske uudestaan</a>
</body>

</html>