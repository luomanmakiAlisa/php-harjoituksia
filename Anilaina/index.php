<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Document</title>
</head>

<body>
  <h3>Annuiteettilaina</h3>
  <form action="laina.php" method="post">
    <div>
      <label for="paaoma">Paaoma</label>
      <input type="number" name="paaoma" step="1" min="0" />
    </div>
    <div>
      <label for="korko">Korko</label>
      <select name="korko">
        <?php
        for ($i = 0.25; $i <= 5; $i = $i + 0.25) {
          printf("<option>%.2f</option>", $i);
        }
        ?>
      </select>
    </div>
    <div>
      <label for="aika">Aikajakso</label>
      <select name="aika">
        <?php
        for ($i = 1; $i <= 30; $i++) {
          print("<option>$i</option>");
        }
        ?>
      </select>
    </div>
    <button>Laske</button>
  </form>
</body>

</html>