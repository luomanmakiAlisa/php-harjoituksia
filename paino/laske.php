<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $pituus = filter_input(INPUT_POST, 'pituus', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $paino = filter_input(INPUT_POST, 'paino', FILTER_SANITIZE_NUMBER_INT);
    $bmi = $paino / ($pituus * $pituus);
    printf("Painoindeksi on %.1f", $bmi);
    ?>
    <a href="index.html">Laske uudestaan</a>

</body>

</html>