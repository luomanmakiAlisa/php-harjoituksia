<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h3>Lämpötilamuunnin</h3>
    <?php
    $arvo = filter_input(INPUT_POST, "fahren", FILTER_SANITIZE_NUMBER_INT);
    $muutettu = ($arvo - 32) / 1.8;
    printf("$arvo on celciusasteina %.2f ", $muutettu);
    ?>
    <a href="index.html">Laske uusi</a>
</body>

</html>