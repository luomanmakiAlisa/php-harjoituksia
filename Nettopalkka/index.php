<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nettopalkka</title>
    <meta content="Alisa Luomanmäki" name="author">
    <link rel="stylesheet" href="css/styles.css" />
</head>

<body>
    <?php
    $nettopalkka = 0;
    $brutto = 0;
    $pidatys = 0;
    $temaksu = 0;
    $tvvakuutus = 0;
    $pidatysTulo = 0;
    $temaksuTulo = 0;
    $tvvakuutusTulo = 0;

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $brutto = filter_input(INPUT_POST, 'brutto', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $pidatys = filter_input(INPUT_POST, 'pidatys', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $temaksu = filter_input(INPUT_POST, 'temaksu', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $tvvakuutus = filter_input(INPUT_POST, 'tvvakuutus', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $pidatysTulo = 0;
        $pidatysTulo = $pidatys * 0.01 * $brutto;
        $temaksuTulo = $temaksu  * 0.01 * $brutto;
        $tvvakuutusTulo = $tvvakuutus * 0.01 * $brutto;
        $nettopalkka = $brutto - ($pidatysTulo + $temaksuTulo + $tvvakuutusTulo);
    }

    ?>
    <div>
        <h3>Nettopalkka</h3>
        <form action="index.php" method="post">
            <section>
                <div>
                    <label for="brutto">Bruttopalkka:</label>
                    <input type="number" name="brutto" step="1" value="<?php printf('%.2f', $brutto) ?>">
                </div>
                <div>
                    <label for="pidatys">Ennakkopidatys:</label>
                    <input type="number" name="pidatys" step="0.01" value="<?php printf('%.2f', $pidatys) ?>">
                    <?php
                    printf("<span>%% %.2f €</span>", $pidatysTulo);
                    ?>
                </div>
                <div>
                    <label for="temaksu">Työeläkemaksu:</label>
                    <input type="number" name="temaksu" step="0.01" value="<?php printf('%.2f', $temaksu) ?>">
                    <?php
                    printf("<span>%% %.2f €</span>", $temaksuTulo);
                    ?>
                </div>
                <div>
                    <label for="tvvakuutus">Työttömyysvakuutusmaksu:</label>
                    <input type="number" name="tvvakuutus" step="0.01" value="<?php printf('%.2f', $tvvakuutus) ?>">
                    <?php
                    printf("<span>%% %.2f €</span>", $tvvakuutusTulo);
                    ?>
                </div>
            </section>
            <button>Laske</button>
        </form>
        <output>
            <?php
            printf("Nettopalkka on %.2f €", $nettopalkka);
            ?>
        </output>
    </div>
</body>

</html>